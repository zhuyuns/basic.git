package mgo

import (
	"fmt"
	"sync"
	"gopkg.in/mgo.v2"
)

var records = make(map[string]*mgo.Database)
var mutex sync.RWMutex

func Create(name, address,dbName string) error {
	mutex.Lock()
	defer mutex.Unlock()

	if _, ok := records[name]; ok {
		return fmt.Errorf("Exist %s", name)
	}

	session, err := mgo.Dial(address)
	if err != nil {
		return err
	}

	session.SetMode(mgo.Monotonic, true)
	c := session.DB(dbName)
	records[name] = c
	return nil
}

func Get(name string) *mgo.Database {
	mutex.RLock()
	m, ok := records[name]
	mutex.RUnlock()

	if !ok {
		return nil
	}

	return m
}