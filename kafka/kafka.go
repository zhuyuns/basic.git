/**
 * @api post kafka.
 *
 * User: yunshengzhu
 * Date: 2021/8/6
 * Time: 上午10:22
 */
package kafka

import (
	"crypto/rand"
	"encoding/json"
	"gitee.com/zhuyuns/basic/logs"
	"github.com/Shopify/sarama"
	"github.com/btcsuite/btcutil/base58"
	"io"
	"reflect"
	"time"
)

type KafkaPusher struct {
	producer sarama.SyncProducer
	address  []string
	pullLog  bool
	pushLog  bool
}

func NewKafkaPusher(address []string) (*KafkaPusher, error) {
	k := &KafkaPusher{address: address}
	err := k.initKafka()
	return k, err
}

//生成长度n的随机字符串，采用base58编码
//主要用于生成酒证地址
func RandString(n int) string {
	buf := make([]byte, n)
	_, er := io.ReadFull(rand.Reader, buf)
	if er != nil {
		panic(er)
	}
	return string([]byte(base58.Encode(buf))[:n])
}
func ToJsonStringIndent(i interface{}) string {
	if IsNil(i) {
		return ""
	}
	bs, er := json.MarshalIndent(i, "", " ")
	if er != nil {
		return ""
	}

	return string(bs)
}

func IsNil(i interface{}) bool {
	vi := reflect.ValueOf(i)
	if vi.Kind() == reflect.Ptr {
		return vi.IsNil()
	}
	return false
}
func (s *KafkaPusher) initKafka() error {
	kafkaConfig := sarama.NewConfig()
	kafkaConfig.Version = sarama.V2_0_0_0
	kafkaConfig.ClientID = RandString(20)
	kafkaConfig.Producer.Retry.Max = 1000000
	kafkaConfig.Producer.Retry.Backoff = 10 * time.Second
	kafkaConfig.Producer.RequiredAcks = sarama.WaitForAll
	kafkaConfig.Producer.Return.Successes = true
	kafkaConfig.Producer.Return.Errors = true
	kafkaConfig.Producer.Partitioner = sarama.NewRandomPartitioner
	kafkaConfig.Producer.MaxMessageBytes = 1000000 * 5
	kafkaConfig.Net.MaxOpenRequests = 1
	var err error
	s.producer, err = sarama.NewSyncProducer(s.address, kafkaConfig)
	if err != nil {
		return err
	}
	return nil
}

func (s *KafkaPusher) Stop() {
	_ = s.producer.Close()
}

func (s *KafkaPusher) SetPullLog(status bool) {
	s.pullLog = status
}

func (s *KafkaPusher) GetPullLog() bool {
	return s.pullLog
}

func (s *KafkaPusher) SetPushLog(status bool) {
	s.pullLog = status
}

func (s *KafkaPusher) GetPushLog() bool {
	return s.pullLog
}

func (s *KafkaPusher) KafkaSend(topic string, message interface{}) error {
	if s.producer == nil {
		logs.Log.Info("初始化kafka链接")
		if err := s.initKafka(); err != nil {
			return err
		}
	}
	if s.producer != nil {
		partition, offset, err := s.producer.SendMessage(&sarama.ProducerMessage{
			Topic: topic,
			Value: sarama.StringEncoder(ToJsonStringIndent(message)),
		})
		if err != nil {
			_ = s.producer.Close()
			s.producer = nil
			return err
		}
		if s.pushLog {
			logs.Log.Infof("推送消息成功 partition:%d, offset:%d [ %s => %s]", partition, offset,
				ToJsonStringIndent(message), topic)
		}
	}
	return nil
}
