package mysql

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"sync"
)

var records = make(map[string]*gorm.DB)
var mutex sync.RWMutex

var ErrRecordNotFound = gorm.ErrRecordNotFound

func Create(name string, dns string, maxIdle, maxOpen, debug int) error {
	mutex.Lock()
	defer mutex.Unlock()

	if _, ok := records[name]; ok {
		return fmt.Errorf("Exist %s", name)
	}

	db, err := gorm.Open("mysql", dns)
	if err != nil {
		panic(err)
	}

	db.DB().SetMaxIdleConns(maxIdle)
	db.DB().SetMaxOpenConns(maxOpen)

	if debug==1 {
		db.LogMode(true)
	}

	records[name] = db
	return nil
}

func Get(name string) *gorm.DB {
	mutex.RLock()
	m, ok := records[name]
	mutex.RUnlock()

	if !ok {
		return nil
	}

	return m
}

func Close(name string) {
	mutex.RLock()
	m, ok := records[name]
	delete(records, name)
	mutex.RUnlock()
	if !ok {
		return
	}
	m.Close()
}
