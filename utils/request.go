/**
 * @api post utils.
 *
 * User: yunshengzhu
 * Date: 2022/1/5
 * Time: 下午1:07
 */
package utils

import (
	"context"
	"github.com/google/uuid"
	"google.golang.org/grpc/metadata"
)

func NewRequestId() context.Context {
	requestId := uuid.New().String()
	param := map[string]string{"requestid": requestId}
	ctx := metadata.NewIncomingContext(context.Background(), metadata.New(param))
	ctx = metadata.AppendToOutgoingContext(ctx, "requestid", requestId)
	return ctx
}
