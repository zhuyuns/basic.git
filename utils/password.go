/**
 * @api post utils.
 *
 * User: yunshengzhu
 * Date: 2021/12/25
 * Time: 下午6:49
 */
package utils

import (
	"fmt"
	"math/rand"
	"time"
)

const (
	NUmStr  = "0123456789"
	CharStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
)

func CreatePwd(n int) string {
	var pwd []byte = make([]byte, n, n)
	var sourceStr string
	rand.Seed(time.Now().UnixNano())
	sourceStr = fmt.Sprintf("%s%s", NUmStr, CharStr)
	for i := 0; i < n; i++ {
		index := rand.Intn(len(sourceStr))
		pwd[i] = sourceStr[index]
	}
	return string(pwd)
}
