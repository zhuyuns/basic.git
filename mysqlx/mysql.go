package mysqlx

import (
	"fmt"
	"sync"
	_ "github.com/go-sql-driver/mysql"
	"xorm.io/core"
	"github.com/go-xorm/xorm"
)

var records = make(map[string]*xorm.Engine)
var mutex sync.RWMutex

func Create(name string, dns string, maxIdleConns, maxOpenConns int, debug bool) error {
	mutex.Lock()
	defer mutex.Unlock()

	if _, ok := records[name]; ok {
		return fmt.Errorf("Exist %s", name)
	}

	engine, err := xorm.NewEngine("mysql", dns)
	if err != nil {
		return err
	}

	engine.SetMaxIdleConns(maxIdleConns)
	engine.SetMaxOpenConns(maxOpenConns)

	if debug {
		engine.ShowSQL(true)
		engine.Logger().SetLevel(core.LOG_DEBUG)
	}

	records[name] = engine
	return nil
}

func Get(name string) *xorm.Engine {
	mutex.RLock()
	m, ok := records[name]
	mutex.RUnlock()

	if !ok {
		return nil
	}

	return m
}

func Close(name string) {
	mutex.RLock()
	m, ok := records[name]
	mutex.RUnlock()
	if !ok {
		return
	}
	m.Close()
}
