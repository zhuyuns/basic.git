package oracle

import (
	"github.com/go-xorm/xorm"
	_ "github.com/mattn/go-oci8"
	"log"
	"strings"
	"time"
)

var defaultConn *xorm.Engine

var dns string
var maxIdleConn, writeMaxConn int
var debug bool

func Create(name, dnsParam string, maxIdleConnParam, writeMaxConnParam int, debugParam bool) error {
	dns = dnsParam
	maxIdleConn = maxIdleConnParam
	writeMaxConn = writeMaxConnParam
	debug = debugParam
	err := conn()
	go startPing()
	return err
}

func conn() (err error) {
	//os.Setenv("NLS_LANG", "AMERICAN_AMERICA.AL32UTF8")
	defaultConn, err = xorm.NewEngine("oci8", dns)
	if err != nil {
		return err
	}
	defaultConn.ShowExecTime(true)
	defaultConn.ShowSQL(debug)
	defaultConn.SetMaxOpenConns(writeMaxConn)
	defaultConn.SetMaxIdleConns(maxIdleConn)
	return
}

func Get(name string) *xorm.Engine {
	return defaultConn
}

func startPing() {
	sec_2 := time.NewTimer(time.Second * 30)
	for {
		select {
		case <-sec_2.C:
			Ping()
			sec_2.Reset(time.Second * 30)
		}
	}
}

func Ping() {
	oracle := defaultConn
	_, err := oracle.Exec(`select 1 from dual`)
	if err != nil {
		errString := err.Error() // 去除换行符
		errString = strings.Replace(errString, "\n", "", -1)
		if strings.EqualFold(errString, "ORA-03114: not connected to ORACLE") || strings.Contains(errString, "ORA-03135: connection lost") {
			log.Println("服务器链接异常 hsh write:", err)
			err := conn()
			if err != nil {
				log.Println("err:", err)
			}
		}
	}
}
