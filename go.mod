module gitee.com/zhuyuns/basic

go 1.16

require (
	github.com/Shopify/sarama v1.34.1
	github.com/btcsuite/btcutil v1.0.2
	github.com/davecgh/go-spew v1.1.1
	github.com/go-sql-driver/mysql v1.6.0
	github.com/go-xorm/xorm v0.7.9
	github.com/golang/protobuf v1.5.2
	github.com/google/uuid v1.3.0
	github.com/jinzhu/gorm v1.9.16
	github.com/json-iterator/go v1.1.12
	github.com/leekchan/accounting v1.0.0
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/mattn/go-oci8 v0.1.1
	github.com/olivere/elastic v6.2.37+incompatible
	github.com/shopspring/decimal v1.3.1
	github.com/sideshow/apns2 v0.23.0
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.8.0
	github.com/syndtr/goleveldb v1.0.0
	github.com/tipycalFlow/pkcs12 v0.0.0-20160122071024-7b34261ccc79
	go.etcd.io/etcd/api/v3 v3.5.4
	go.etcd.io/etcd/client/v3 v3.5.4
	golang.org/x/crypto v0.0.0-20220622213112-05595931fe9d
	golang.org/x/net v0.0.0-20220708220712-1185a9018129
	google.golang.org/grpc v1.47.0
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
	gorm.io/driver/mysql v1.3.5
	gorm.io/gorm v1.23.8
	xorm.io/core v0.7.3
)
