package clog

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path"
	"sync"
)

var (
	_map          map[string]string
	_lock         sync.RWMutex
	_file         File
	_default_file = path.Join(path.Join(path.Dir(os.Args[0]), "./conf"), "app.conf")
	_config_file  = flag.String("config", _default_file, "config filename")
	_force        = flag.Bool("f", false, "force")
)

func init() {
	_file = make(File)
}

func Get(name string) map[string]string {
	_lock.RLock()
	defer _lock.RUnlock()
	return _file.Section(name)
}



func _load_config(tpath string) (File, error) {
	var err error
	log.Println("Loading Config.")
	file, err := LoadFile(tpath)
	if err != nil {
		fmt.Printf("load [%s] error: %s", tpath, err)
	}
	return file, err
}

func GetLogLevel() int {
	return _log_level
}
