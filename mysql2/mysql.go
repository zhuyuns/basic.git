/**
 * @api post mysql2.
 *
 * User: yunshengzhu
 * Date: 2022/5/7
 * Time: 11:43 PM
 */
package mysql2

import (
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
	"os"
	"sync"
	"time"
)

var records = make(map[string]*gorm.DB)
var mutex sync.RWMutex
var ErrRecordNotFound = gorm.ErrRecordNotFound

type MySqlGorm struct {
	dsn      string
	maxIdle  int
	maxOpen  int
	slowTime time.Duration
}

type MysqlOption func(db *MySqlGorm)

func WithMaxIdle(maxIdle int) MysqlOption {
	return func(db *MySqlGorm) {
		db.maxIdle = maxIdle
	}
}

func WithMaxOpen(maxOpen int) MysqlOption {
	return func(db *MySqlGorm) {
		db.maxOpen = maxOpen
	}
}

func WithSlowTime(duration time.Duration) MysqlOption {
	return func(db *MySqlGorm) {
		db.slowTime = duration
	}
}

func New(name string, dns string, options ...MysqlOption) error {
	mutex.Lock()
	defer mutex.Unlock()
	if _, ok := records[name]; ok {
		return fmt.Errorf("exist %s", name)
	}

	config := &MySqlGorm{
		dsn:      dns,
		maxIdle:  5,
		maxOpen:  200,
		slowTime: time.Second,
	}
	for _, option := range options {
		option(config)
	}
	db, err := config.gormMysql()
	if err != nil {
		return err
	}
	records[name] = db
	return nil
}

func Get(name string) *gorm.DB {
	mutex.RLock()
	m, ok := records[name]
	mutex.RUnlock()
	if !ok {
		return nil
	}
	return m
}

func Close(name string) {
	mutex.RLock()
	m, ok := records[name]
	delete(records, name)
	mutex.RUnlock()
	if !ok {
		return
	}
	db, _ := m.DB()
	db.Close()
}

func (m *MySqlGorm) gormMysql() (*gorm.DB, error) {
	mysqlConfig := mysql.Config{
		DSN:                       m.dsn, // DSN data source name
		DefaultStringSize:         200,   // string 类型字段的默认长度
		SkipInitializeWithVersion: false, // 根据版本自动配置

	}
	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer（日志输出的目标，前缀和日志包含的内容——译者注）
		logger.Config{
			SlowThreshold:             m.slowTime,  // 慢 SQL 阈值
			LogLevel:                  logger.Warn, // 日志级别
			IgnoreRecordNotFoundError: true,        // 忽略ErrRecordNotFound（记录未找到）错误
			Colorful:                  false,       // 禁用彩色打印
		},
	)
	if db, err := gorm.Open(mysql.New(mysqlConfig), &gorm.Config{
		Logger: newLogger,
	}); err != nil {
		return nil, err
	} else {

		sqlDB, err := db.DB()
		if err != nil {
			return nil, err
		}

		sqlDB.SetMaxIdleConns(m.maxIdle)
		sqlDB.SetMaxOpenConns(m.maxOpen)
		sqlDB.SetConnMaxLifetime(time.Hour)
		return db, nil
	}
}
