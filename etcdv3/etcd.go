/**
 * @api post etcdservice.
 *
 * User: yunshengzhu
 * Date: 2019-07-24
 * Time: 21:39
 */
package etcdv3

import (
	"crypto/tls"
	"crypto/x509"
	"go.etcd.io/etcd/client/v3"
	"io/ioutil"
	"time"
)

type TlsConfig struct {
	TlsStatus bool
	CertPath  string
	KeyPath   string
	CaPath    string
}

type Etcd struct {
	client    *clientv3.Client
	tlsConfig *TlsConfig
	endpoints []string
	namespace string
	ttl       time.Duration
}

var Namespace string

func (e *Etcd) Close() error {
	return e.client.Close()
}

func NewEtcd(endpoints []string, username, password, namespace string, ttl int64, tlsConfig *TlsConfig) (*Etcd, error) {
	var err error
	etcd := &Etcd{}
	etcd.tlsConfig = tlsConfig
	etcd.ttl = time.Duration(ttl) * time.Second
	etcd.endpoints = endpoints
	etcd.namespace = namespace
	if etcd.tlsConfig != nil {
		cert, err := tls.LoadX509KeyPair(etcd.tlsConfig.CertPath, etcd.tlsConfig.KeyPath)
		if err != nil {
			return nil, err
		}
		caData, err := ioutil.ReadFile(etcd.tlsConfig.CaPath)
		if err != nil {
			return nil, err
		}
		pool := x509.NewCertPool()
		pool.AppendCertsFromPEM(caData)
		_tlsConfig := &tls.Config{
			Certificates: []tls.Certificate{cert},
			RootCAs:      pool,
		}
		etcd.client, err = clientv3.New(clientv3.Config{
			Endpoints:   etcd.endpoints,
			DialTimeout: etcd.ttl,
			TLS:         _tlsConfig,
			Username:    username,
			Password:    password,
		})
		if err != nil {
			return nil, err
		}
	} else {
		etcd.client, err = clientv3.New(clientv3.Config{
			Endpoints:   etcd.endpoints,
			DialTimeout: etcd.ttl,
			Username:    username,
			Password:    password,
		})
		if err != nil {
			return nil, err
		}
	}
	return etcd, nil
}
