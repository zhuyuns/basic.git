/**
 * @api post etcdv3.
 *
 * User: yunshengzhu
 * Date: 2021/12/19
 * Time: 上午11:08
 */
package etcdv3

import (
	"context"
	"fmt"
	"gitee.com/zhuyuns/basic/middleware"
	"google.golang.org/grpc"
	"google.golang.org/grpc/balancer/roundrobin"
	"google.golang.org/grpc/credentials/insecure"
	"sync"
)

var connMap = make(map[string]*grpc.ClientConn, 0)
var connMx sync.Mutex

func NewRpcConn(ctx context.Context, scheme, service string, opts []grpc.DialOption) (*grpc.ClientConn, error) {
	if conn, ok := connMap[service]; ok {
		return conn, nil
	}
	connMx.Lock()
	defer connMx.Unlock()
	opts = append(opts, grpc.WithDefaultServiceConfig(fmt.Sprintf(`{"LoadBalancingPolicy": "%s"}`, roundrobin.Name)))
	opts = append(opts, grpc.WithUnaryInterceptor(middleware.Interceptor))
	opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))
	conn, err := grpc.DialContext(ctx, scheme+"://authority/"+service, opts...)
	if err == nil {
		connMap[service] = conn
	}
	return conn, err
}
