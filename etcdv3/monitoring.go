/**
 * @api post etcdv3.
 *
 * User: yunshengzhu
 * Date: 2022/7/23
 * Time: 16:58
 */
package etcdv3

import (
	"context"
	"fmt"
	"go.etcd.io/etcd/api/v3/mvccpb"
	"go.etcd.io/etcd/client/v3"
	"time"
)

type MonitoringApp struct {
	prefix       string
	namespace    string
	etcd         *Etcd
	online       func([]byte)
	offline      func([]byte)
	applications func(string, *clientv3.GetResponse)
}

func NewMonitoringApp(etcd *Etcd, namespace string, fix string, online, offline func([]byte), apps func(string, *clientv3.GetResponse)) *MonitoringApp {
	return &MonitoringApp{etcd: etcd, namespace: namespace, prefix: fix, online: online, offline: offline, applications: apps}
}

func (app *MonitoringApp) Watch() {
	watchCh := app.etcd.client.Watch(context.Background(), app.prefix, clientv3.WithPrefix())
	num := 0
	ticker := time.NewTicker(time.Minute * 10)
	for {
		select {
		case res := <-watchCh:
			if res.Err() != nil {
				time.Sleep(time.Second * 2)
				if num > 10 {
					panic(res.Err())
				}
				num++
			} else {
				app.update(res.Events)
				num = 0
			}
		case <-ticker.C:
			app.getApps()
			ticker.Reset(time.Minute * 10)
		}
	}
}

func (app *MonitoringApp) getApps() {
	resp, err := app.etcd.client.Get(context.Background(), app.prefix, clientv3.WithPrefix())
	if err != nil {
		fmt.Println("err:", err)
	}
	app.applications(app.namespace, resp)
}

func (app *MonitoringApp) update(events []*clientv3.Event) {
	for _, ev := range events {
		switch ev.Type {
		case mvccpb.PUT:
			app.online(ev.Kv.Value)
			app.getApps()
		case mvccpb.DELETE:
			app.offline(ev.Kv.Key)
			app.getApps()
		}
	}
}
