/**
 * @api post etcdservice.
 *
 * User: yunshengzhu
 * Date: 2019-07-31
 * Time: 14:36
 */
package etcdv3

import (
	"context"
	"fmt"
	"go.etcd.io/etcd/client/v3"
)

type EtcdMutex struct {
	etcd    *Etcd
	Ttl     int64              //租约时间
	Key     string             //etcd的key
	cancel  context.CancelFunc //关闭续租的func
	lease   clientv3.Lease
	leaseID clientv3.LeaseID
	txn     clientv3.Txn
}

func NewEtcdMutex(etcd *Etcd, ttl int64, key string) (*EtcdMutex, error) {
	etcdMutex := &EtcdMutex{
		etcd: etcd,
		Ttl:  ttl,
		Key:  key,
	}
	err := etcdMutex.init()
	if err != nil {
		return nil, err
	}
	return etcdMutex, nil
}

func (em *EtcdMutex) init() error {
	var err error
	var ctx context.Context
	em.txn = clientv3.NewKV(em.etcd.client).Txn(context.TODO())
	em.lease = clientv3.NewLease(em.etcd.client)
	leaseResp, err := em.lease.Grant(context.TODO(), em.Ttl)
	if err != nil {
		return err
	}
	ctx, em.cancel = context.WithCancel(context.TODO())
	em.leaseID = clientv3.LeaseID(leaseResp.ID)
	_, err = em.lease.KeepAlive(ctx, em.leaseID)
	return err
}

func (em *EtcdMutex) Lock() error {
	err := em.init()
	if err != nil {
		return err
	}
	//LOCK:
	em.txn.If(clientv3.Compare(clientv3.CreateRevision(em.Key), "=", 0)).
		Then(clientv3.OpPut(em.Key, "", clientv3.WithLease(em.leaseID))).
		Else()
	txnResp, err := em.txn.Commit()
	if err != nil {
		return err
	}
	if !txnResp.Succeeded { //判断txn.if条件是否成立
		return fmt.Errorf("抢锁失败")
	}
	return nil
}

func (em *EtcdMutex) UnLock() {
	em.cancel()
	em.lease.Revoke(context.TODO(), em.leaseID)
	fmt.Println("释放了锁")
}
