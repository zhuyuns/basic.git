/**
 * @api post myContext.
 *
 * User: yunshengzhu
 * Date: 2022/5/1
 * Time: 1:30 PM
 */
package context

import (
	c "context"
	"gitee.com/zhuyuns/basic/logs"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc/metadata"
)

func NewContext(ctx c.Context) Context {
	myCtx := Context{}
	myCtx.Context = ctx
	md, _ := metadata.FromIncomingContext(ctx)
	myCtx.AppId = getKey(md, "appid")
	myCtx.ReqId = getKey(md, "reqid")
	myCtx.ReqTime = getKey(md, "reqtime")

	myCtx.Log = logs.Log.WithField("reqId", myCtx.ReqId)
	if logs.Log.ServerName != "" {
		myCtx.Log = myCtx.Log.WithField("serverName", logs.Log.ServerName)
	}
	return myCtx
}

func getKey(md metadata.MD, key string) string {
	reqIds := md[key]
	reqId := ""
	if len(reqIds) > 0 {
		reqId = reqIds[0]
	}
	return reqId
}

type Context struct {
	c.Context
	AppId          string
	ReqId          string
	ReqTime        string
	AppCallBackUrl string
	Log            *log.Entry
}

func (my *Context) GetContext() c.Context {
	ctx := c.Background()
	md := make(map[string]string)
	md["appid"] = my.AppId
	md["reqid"] = my.ReqId
	md["reqtime"] = my.ReqTime
	ctx = metadata.NewIncomingContext(ctx, metadata.New(md))
	return ctx
}
