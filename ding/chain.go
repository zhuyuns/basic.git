/**
 * @api post model.
 *
 * User: yunshengzhu
 * Date: 2020/7/31
 * Time: 3:26 下午
 */
package ding

import (
	"context"
	"fmt"
	jsoniter "github.com/json-iterator/go"
	"io/ioutil"
	"net/http"
	"gitee.com/zhuyuns/basic/cfg"
	"time"
)

type ChainResp struct {
	Code int `json:"code"`
	Data struct {
		Count int64 `json:"count"`
		List  []struct {
			BlockHeight int64 `json:"blockHeight"`
		} `json:"list"`
	} `json:"data"`
}

func GetBlockHeight(url string) (int64, error) {
	var (
		ctx    context.Context
		cancel context.CancelFunc
		resp   *http.Response
		err    error
	)
	ctx, cancel = context.WithTimeout(context.Background(), time.Second*2)
	defer cancel()
	header := map[string]string{
		"Content-type": "application/json",
	}
	resp, err = doRequest(ctx, "GET", url, header, nil)
	if err != nil {
		return 0, err
	}
	if resp.StatusCode != http.StatusOK {
		body, _ := ioutil.ReadAll(resp.Body)
		return 0, fmt.Errorf("err: %s", string(body))
	}
	body, _ := ioutil.ReadAll(resp.Body)
	chainResp := ChainResp{}
	err = jsoniter.Unmarshal(body, &chainResp)
	if err != nil {
		return 0, err
	}

	if len(chainResp.Data.List) <= 0 {
		fmt.Println("区块数据为0")
		return 0, nil
	}
	return chainResp.Data.List[0].BlockHeight, nil
}

type block struct {
	Code int `json:"code"`
	Data struct {
		Count int64 `json:"count"`
		List  []struct {
			BlockHeight int64  `json:"blockNumber"`
			FuncName    string `json:"funcName"`
			TxId        string `json:"txId"`
			Timestamp   int64  `json:"timestamp"`
		} `json:"list"`
	} `json:"data"`
}

func GetBlockHeightNumber(url string, heightBlock int64) (b block, err error) {
	var (
		ctx    context.Context
		cancel context.CancelFunc
		resp   *http.Response
	)
	ctx, cancel = context.WithTimeout(context.Background(), time.Second*2)
	defer cancel()
	header := map[string]string{
		"Content-type": "application/json",
	}
	resp, err = doRequest(ctx, "GET", url+fmt.Sprintf("/queryTxs?startTime=&endTime=&blockNum=%d&page=1&size=10", heightBlock), header, nil)
	if err != nil {
		return b, err
	}
	if resp.StatusCode != http.StatusOK {
		body, _ := ioutil.ReadAll(resp.Body)
		return b, fmt.Errorf("err: %s", string(body))
	}
	body, _ := ioutil.ReadAll(resp.Body)

	err = jsoniter.Unmarshal(body, &b)
	if err != nil {
		return b, err
	}

	return b, nil
}

type TokenTxIdResp struct {
	Code int `json:"code"`
	Data struct {
		Count int64 `json:"count"`
	} `json:"data"`
}

func GetTokenTxId(url string, txId string) (b TokenTxIdResp, err error) {
	var (
		ctx    context.Context
		cancel context.CancelFunc
		resp   *http.Response
	)
	ctx, cancel = context.WithTimeout(context.Background(), time.Second*2)
	defer cancel()
	header := map[string]string{
		"Content-type": "application/json",
	}

	urlStr := url + fmt.Sprintf("/queryTokenHistory?address=&txId=%s&page=1&size=10", txId)

	cfg.LogInfo("请求地址:", urlStr)

	resp, err = doRequest(ctx, "GET", urlStr, header, nil)
	if err != nil {
		return b, err
	}
	if resp.StatusCode != http.StatusOK {
		body, _ := ioutil.ReadAll(resp.Body)
		return b, fmt.Errorf("err: %s", string(body))
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return b, err
	}
	cfg.LogInfo("body:", string(body))
	err = jsoniter.Unmarshal(body, &b)
	if err != nil {
		return b, err
	}
	return b, nil
}
