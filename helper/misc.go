package helper

import "math"

// In32  检查数字是否在, a数组中
func In32(a []int32, i int32) bool {
	for _, item := range a {
		if item == i {
			return true
		}
	}

	return false
}

func In64(a []int64, i int64) bool {
	for _, item := range a {
		if item == i {
			return true
		}
	}

	return false
}

func Int32ToInt64(a []int32) []int64 {
	data := make([]int64, len(a))
	for i, v := range a {
		data[i] = int64(v)
	}

	return data
}

// 四舍五入
func Round(f float64, prec int) float64 {
	pow10N := math.Pow10(prec)
	return math.Floor(f*pow10N+0.5) / pow10N
}

func MaxInt32(i, j int32) int32 {
	if i > j {
		return i
	}

	return j
}
