package helper

import (
	"os"
	"runtime"

	log "gitee.com/zhuyuns/basic/cfg"
	"github.com/davecgh/go-spew/spew"
)

// RecoverStack 产生panic时的调用栈打印
func RecoverStack(extras ...interface{}) {
	if x := recover(); x != nil {
		log.LogErr(x)
		i := 0
		funcName, file, line, ok := runtime.Caller(i)
		for ok {
			log.LogErrf("frame %v:[func:%v,file:%v,line:%v]\n", i, runtime.FuncForPC(funcName).Name(), file, line)
			i++
			funcName, file, line, ok = runtime.Caller(i)
		}

		for k := range extras {
			log.LogErrf("EXRAS#%v DATA:%v\n", k, spew.Sdump(extras[k]))
		}
	}
}

func RecoverStackPanic(extras ...interface{}) {
	if x := recover(); x != nil {
		log.LogErr(x)
		i := 0
		funcName, file, line, ok := runtime.Caller(i)
		for ok {
			log.LogErrf("frame %v:[func:%v,file:%v,line:%v]\n", i, runtime.FuncForPC(funcName).Name(), file, line)
			i++
			funcName, file, line, ok = runtime.Caller(i)
		}

		for k := range extras {
			log.LogErrf("EXRAS#%v DATA:%v\n", k, spew.Sdump(extras[k]))
		}

		panic(x)
	}
}

func PainErr(err error) {
	if err != nil {
		log.LogFatal(err)
		os.Exit(-1)
	}
}

func PainErrExit(err error) {
	if err != nil {
		log.LogFatal(err)
		os.Exit(0)
	}
}
